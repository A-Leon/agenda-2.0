package com.example.agendasql;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import com.example.agendasql.database.AgendaDbHelper;

public class MainActivity extends AppCompatActivity {

    private TextView lblRespuesta;
    private Button btnRespuesta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblRespuesta= (TextView) findViewById(R.id.lblRespuesta);
        btnRespuesta = (Button) findViewById(R.id.btnProbar);

        btnRespuesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dataBaseName= "";

                SQLiteDatabase db;
                AgendaDbHelper helper = new AgendaDbHelper(MainActivity.this);
                db= helper.getWritableDatabase();
                helper.onUpgrade(db,1,2);
                dataBaseName = helper.getDatabaseName();

                lblRespuesta.setText(dataBaseName);


            }
        });
    }
}
